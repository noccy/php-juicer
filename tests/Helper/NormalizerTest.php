<?php

namespace NoccyLabs\Juicer\Helper;


class NormalizerTest extends \PhpUnit\Framework\TestCase
{

    public function testThatNormalizedPercentagesAreSane()
    {
        $normalizer = new Normalizer([
            'A' => 10
        ]);

        $this->assertEquals(10, $normalizer->getPercent("A"));
        $this->assertEquals(100, $normalizer->getNormalizedPercent("A"));

        $normalizer = new Normalizer([
            'A' => 10,
            'B' => 10,
        ]);

        $this->assertEquals(10, $normalizer->getPercent("A"));
        $this->assertEquals(50, $normalizer->getNormalizedPercent("A"));
        $this->assertEquals(10, $normalizer->getPercent("B"));
        $this->assertEquals(50, $normalizer->getNormalizedPercent("B"));

        $normalizer = new Normalizer([
            'A' => 2,
            'B' => 8,
        ]);

        $this->assertEquals(20, $normalizer->getNormalizedPercent("A"));
        $this->assertEquals(80, $normalizer->getNormalizedPercent("B"));

    }
}
