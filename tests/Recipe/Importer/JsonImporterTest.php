<?php

namespace NoccyLabs\Juicer\Recipe\Importer;

use NoccyLabs\Juicer\Recipe\Recipe;
use NoccyLabs\Juicer\Ingredient\Ingredient;
use NoccyLabs\Juicer\Recipe\Exporter\JsonExporter;
use NoccyLabs\Juicer\Recipe\RecipeInterface;

class JsonImporterTest extends \PhpUnit\Framework\TestCase
{
    public function testThatRecipesCanBeImportedFromJson()
    {

        $json = '{ "recipe":"foo", "author":"bar", "ingredients": [{"flavor":"Cherry", "brand":"FA", "percent":1}] }';

        $importer = new JsonImporter();
        $recipe = $importer->import($json);

        $this->assertEquals("foo", $recipe->getRecipeName());
        $this->assertEquals("bar", $recipe->getRecipeAuthor());

        $this->assertCount(1, $recipe->getIngredients());

    }

    public function testThatRecipesCanBeImportedFromExportedJson()
    {

        $recipe = new Recipe();
        $recipe->setRecipeName("foo");
        $recipe->setRecipeAuthor("bar");
        $recipe->addIngredient(new Ingredient("Cherry", "FA", 1));
        $recipe->addIngredient(new Ingredient("Vanilla Swirl", "TFA", 2));

        $exporter = new JsonExporter();
        $exported = $exporter->export($recipe);

        $importer = new JsonImporter();
        $importedRecipe = $importer->import($exported);

        $this->assertEquals($recipe, $importedRecipe);
    }

    public function testThatRecipesCanBeImportedFromFile()
    {

        $importer = new JsonImporter();
        $importedRecipe = $importer->readFromFile(__DIR__."/../../data/recipe1.json");

        $this->assertInstanceOf(RecipeInterface::class, $importedRecipe);
        $this->assertEquals("Recipe 1", $importedRecipe->getRecipeName());
    }

}