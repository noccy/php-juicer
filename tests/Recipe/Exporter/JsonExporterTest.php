<?php

namespace NoccyLabs\Juicer\Recipe\Exporter;

use NoccyLabs\Juicer\Recipe\Recipe;
use NoccyLabs\Juicer\Ingredient\Ingredient;

class JsonExporterTest extends \PhpUnit\Framework\TestCase
{
    public function testThatRecipesCanBeExportedToJson()
    {
        $recipe = new Recipe();
        $recipe->setRecipeName("foo");
        $recipe->setRecipeAuthor("bar");

        $this->assertInstanceOf(Recipe::class, $recipe);
        $this->assertEquals("foo", $recipe->getRecipeName());
        $this->assertEquals("bar", $recipe->getRecipeAuthor());

        $exporter = new JsonExporter();

        $exported = $exporter->export($recipe);
        $expected = json_encode([
            'recipe' => 'foo',
            'author' => 'bar',
            'tags' => [],
            'description' => null,
            'extra' => [],
            'ingredients' => []
        ], JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);

        $this->assertEquals($expected, $exported);
    }

    public function testThatRecipesWithIngredientsCanBeExportedToJson()
    {
        $recipe = new Recipe();
        $recipe->setRecipeName("foo");
        $recipe->setRecipeAuthor("bar");
        $recipe->addIngredient(new Ingredient("Cherry", "FA", 1));
        $recipe->addIngredient(new Ingredient("Vanilla Swirl", "TFA", 2));

        $this->assertInstanceOf(Recipe::class, $recipe);
        $this->assertEquals("foo", $recipe->getRecipeName());
        $this->assertEquals("bar", $recipe->getRecipeAuthor());

        $exporter = new JsonExporter();

        $exported = $exporter->export($recipe);
        $expected = json_encode([
            'recipe' => 'foo',
            'author' => 'bar',
            'tags' => [],
            'description' => null,
            'extra' => [],
            'ingredients' => [
                [ 'brand' => 'FA', 'flavor' => 'Cherry', 'percent' => 1 ],
                [ 'brand' => 'TFA', 'flavor' => 'Vanilla Swirl', 'percent' => 2 ]
            ]
        ], JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);

        $this->assertEquals($expected, $exported);
    }

}