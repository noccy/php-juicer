<?php

namespace NoccyLabs\Juicer\Recipe\Mixer;

use NoccyLabs\Juicer\Recipe\Recipe;
use NoccyLabs\Juicer\Ingredient\Ingredient;
use NoccyLabs\Juicer\Ingredient\Base;
use NoccyLabs\Juicer\Recipe\RecipeInterface;

class MixedRecipeTest extends \PhpUnit\Framework\TestCase
{

        public function getValuesForPropertyTest()
        {
            $ingredientA = new Ingredient("Ingredient A", null, 2);
            $ingredientB = new Ingredient("Ingredient B", null, 3);
            $ingredientC = new Ingredient("Ingredient B", null, 4);

            $recipe1 = new Recipe();
            $recipe1->setRecipeName("Recipe 1");
            $recipe1->addIngredient($ingredientA);
            $recipe1->addIngredient($ingredientB);
            $percent1 = 5;

            $recipe2 = new Recipe();
            $recipe2->setRecipeName("Recipe 2");
            $recipe2->addIngredient($ingredientB);
            $recipe2->addIngredient($ingredientC);
            $percent2 = 7;
    
            $base1 = new Base("VG70");
            $base2 = new Base("VG50");

            $ml1 = 60;
            $ml2 = 30;

            $g1_1 = 0;
            $g1_2 = 0;
            $g2_1 = 0;
            $g2_2 = 0;

            return [
                [ $recipe1, $base1, $percent1, $ml1, $g1_1 ],
                [ $recipe1, $base2, $percent1, $ml1, $g1_2 ],
                [ $recipe2, $base1, $percent2, $ml2, $g2_1 ],
                [ $recipe2, $base2, $percent2, $ml2, $g2_2 ],
            ];
        }

        /**
         * @dataProvider getValuesForPropertyTest
         */
        public function testCalculatedPropertiesOfMixedRecipe(Recipe $recipe, $base, $expectedPercent, $expectedMl, $expectedG)
        {
            $mixer = new Mixer();
            $mixed = $mixer->mixRecipe($recipe, $expectedMl, $base, 0, null);

            $this->assertEquals($expectedMl, $mixed->getVolume());
            $this->assertEquals($expectedPercent, $mixed->getTotalFlavorPercent());
        }

}