<?php

namespace NoccyLabs\Juicer\Recipe\Mixer;

use NoccyLabs\Juicer\Recipe\Recipe;
use NoccyLabs\Juicer\Ingredient\Ingredient;
use NoccyLabs\Juicer\Ingredient\Base;
use NoccyLabs\Juicer\Recipe\RecipeInterface;

class MixerTest extends \PhpUnit\Framework\TestCase
{

    public function getDataForEmptyRecipes()
    {
        return [
            [ 'VG100', 10, [ 'VG' => 10 ], 10 ],
            [ 'PG100', 10, [ 'PG' => 10 ], 10 ],
            [ 'VG70', 10, [ 'VG' => 7, 'PG' => 3 ], 10 ],
            [ 'VG50', 10, [ 'VG' => 5, 'PG' => 5 ], 10 ]
        ];
    }

    /**
     * @dataProvider getDataForEmptyRecipes
     */
    public function testMixingEmptyRecipes($base, $amount, array $assertBases, $assertAmount)
    {
        $recipe = new Recipe();
        $mixer = new Mixer();

        $base = new Base($base);
        $mixed = $mixer->mixRecipe($recipe, $amount, $base, 0)->getMeasuredIngredients();

        $this->assertCount(count($assertBases), $mixed);
        $mixedAmount = 0;
        foreach ($mixed as $measured) {
            $name = $measured->getFlavorName();
            if (!array_key_exists($name, $assertBases)) {
                $this->assertFail("Mixed contains unexpected ingredients");
            }
            $mixedAmount += $measured->getVolume();
        }
        $this->assertEquals($assertAmount, $mixedAmount);
    }

    public function getDataForDummyRecipes()
    {
        $ingredientA1 = new Ingredient("Ingredient A1", null, 2);
        $ingredientA2 = new Ingredient("Ingredient A2", null, 3);
        $recipeA = new Recipe();
        $recipeA->setRecipeName("Recipe A");
        $recipeA->addIngredient($ingredientA1);
        $recipeA->addIngredient($ingredientA2);
        return [
            [ $recipeA, 'VG70', 10 ],
        ];
    }

    /**
     * @dataProvider getDataForDummyRecipes
     */
    public function testMixingDummyRecipes(RecipeInterface $recipe, $base, $amount)
    {
        $mixer = new Mixer();
        $base = new Base($base);
        $mixed = $mixer->mixRecipe($recipe, $amount, $base, 0)->getMeasuredIngredients();

        $expectedCount = count($base->getComponents()) + count($recipe->getIngredients());

        $this->assertCount($expectedCount, $mixed);
        $mixedAmount = 0;
        foreach ($mixed as $measured) {
            $mixedAmount += $measured->getVolume();
        }
        $this->assertEquals($amount, $mixedAmount);
    }

}