<?php

namespace NoccyLabs\Juicer\Recipe;

use NoccyLabs\Juicer\Ingredient\Ingredient;

class RecipeTest extends \PhpUnit\Framework\TestCase
{

    public function testCreatingAndModifyingRecipes()
    {
        $recipe = new Recipe();
        $recipe->setRecipeName("foo");
        $recipe->setRecipeAuthor("bar");
        
        $ingredient1 = new Ingredient("Ingredient 1");
        $ingredient2 = new Ingredient("Ingredient 2");

        $recipe->addIngredient($ingredient1);
        $recipe->addIngredient($ingredient2);

        $this->assertInstanceOf(Recipe::class, $recipe);
        $this->assertEquals("foo", $recipe->getRecipeName());
        $this->assertEquals("bar", $recipe->getRecipeAuthor());
        $this->assertEquals([$ingredient1, $ingredient2], $recipe->getIngredients());
    }

    public function testManipulatingRecipeTags()
    {
        $recipe = new Recipe();

        $recipe
            ->addTag("foo")
            ->addTag("bar");
        
        $this->assertCount(2, $recipe->getTags());
        $this->assertTrue($recipe->hasTag("foo"));
        $this->assertTrue($recipe->hasTag("bar"));
        $recipe->removeTag("foo");
        $this->assertCount(1, $recipe->getTags());
        $this->assertTrue($recipe->hasTag("bar"));

        $recipe->addTag("baz");
        $this->assertCount(2, $recipe->getTags());
        $this->assertFalse($recipe->hasTag("foo"));
        $this->assertTrue($recipe->hasTag("bar"));
        $this->assertTrue($recipe->hasTag("baz"));

        $recipe->setTags(["baz"]);
        $this->assertCount(1, $recipe->getTags());
        $this->assertFalse($recipe->hasTag("foo"));
        $this->assertFalse($recipe->hasTag("bar"));
        $this->assertTrue($recipe->hasTag("baz"));

    }

}