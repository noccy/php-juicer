<?php

namespace NoccyLabs\Juicer\Ingredient;


class NicotineBaseTest extends \PhpUnit\Framework\TestCase
{
    public function getNicotineAsgData()
    {
        return [
            [ 'PG100', 100, 1.035 ],
            [ 'PG100', 60, 1.03632 ],
            [ 'PG100', 50, 1.0366 ],
            [ 'PG100', 48, 1.036656 ],
            [ 'PG100', 36, 1.036992 ],
            [ 'PG100', 24, 1.037328 ],
            [ 'VG100', 100, 1.235 ],
            [ 'VG100', 60, 1.245 ],
            [ 'VG100', 50, 1.2475 ],
            [ 'VG100', 48, 1.248 ],
            [ 'VG100', 36, 1.251 ],
            [ 'VG100', 24, 1.254 ],
        ];
    }

    /**
     * @dataProvider getNicotineAsgData
     */
    public function testThatTheApparentSpecificGravityIsCorect($base, $strength, $expected)
    {
        $nic = new NicotineBase(new Base($base), $strength);
        $this->assertEquals(round($expected,3), round($nic->getSpecificGravity(),3));
    }

}
