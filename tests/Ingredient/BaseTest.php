<?php

namespace NoccyLabs\Juicer\Ingredient;


class BaseTest extends \PhpUnit\Framework\TestCase
{
    public function getBaseAsgData()
    {
        return [
            [ 'PG100', Base::MASS_PG ],
            [ 'VG100', Base::MASS_VG ],
            // NOTE: The internet claims we should get (for 50/50;) 1.1425g/mL
            // but calculating a 50/50 by volume base gives 1.149g/mL.
            [ 'VG50', 1.149 ]
        ];
    }

    /**
     * @dataProvider getBaseAsgData
     */
    public function testThatTheBaseHasTheProperApparentSpecificGravity($base, $expected)
    {
        $base = new Base($base);
        $this->assertEquals($expected, $base->getSpecificGravity());
    }

}