<?php

namespace NoccyLabs\Juicer\Ingredient;

class Base
{
    const MASS_PG = 1.038;
    const MASS_VG = 1.26;

    protected static $MASS = [
        'PG' => self::MASS_PG,
        'VG' => self::MASS_VG,
    ];

    protected $base;

    protected $components = [];

    protected $specificGravity;

    public function __construct(string $base)
    {
        $this->components = self::parseComponents($base);
        $this->base = $base;
        $this->specificGravity = self::calculateSpecificGravity($this->components);
    }

    public function getSpecificGravity(): float
    {
        return $this->specificGravity;
    }

    public function getComponents(): array
    {
        return $this->components;
    }

    public function getComponentPercent(string $component): float
    {
        return array_key_exists($component, $this->components)
            ? $this->components[$component]
            : 0;
    }

    public static function parseComponents(string $base)
    {
        $found = [];
        if (!preg_match('/^([A-Z]+)([0-9]{1,3}+)$/i', $base, $match)) {
            throw new \Exception();
        }

        if ($match[1] == "PG") {
            return [
                'PG' => $match[2],
                'VG' => 100 - $match[2]
            ];
        } elseif ($match[1] == "VG") {
            return [
                'PG' => 100 - $match[2],
                'VG' => $match[2]
            ];
        }
    }

    public static function calculateSpecificGravity(array $components): float
    {
        $specificGravity = 0.0;
        foreach ($components as $component=>$percent) {
            $percentFloat = $percent / 100;
            $specificGravity += (self::$MASS[$component] * $percentFloat);
        }
        return $specificGravity;
    }

    public function __toString()
    {
        return $this->base;
    }
}

