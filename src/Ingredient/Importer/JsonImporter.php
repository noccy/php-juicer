<?php

namespace NoccyLabs\Juicer\Ingredient\Importer;

use NoccyLabs\Juicer\Ingredient\Ingredient;
use NoccyLabs\Juicer\Ingredient\IngredientInterface;

class JsonImporter
{
    /**
     * Import ingredient from json
     * 
     * @param string The json string to parse and import
     * @return IngredientInterface
     */
    public function import(string $json): IngredientInterface
    {
        $data = json_decode($json);

        $ingredient = new Ingredient($data->flavoringName, $data->brandKey);

        print_r($data);

        return $ingredient;
    }

    /**
     * Import ingredient from json contained in a file
     * 
     * @param string The filename to read and import
     * @return IngredientInterface
     */
    public function readFromFile(string $filename): IngredientInterface
    {
        $fd = fopen($filename, "r");
        if (!$fd) {
            throw new \InvalidArgumentException();
        }
        $json = fread($fd, filesize($filename));
        fclose($fd);

        return $this->import($json);
    }

}