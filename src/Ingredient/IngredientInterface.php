<?php

namespace NoccyLabs\Juicer\Ingredient;

interface IngredientInterface
{

    /**
     * Return the name of the flavor
     * 
     * @return string The name of the flavor
     */
    public function getFlavorName(): string;

    /**
     * Return the short name of the flavor brand. This should be the upper-case
     * abbreviation, and not the complete name.
     * 
     * @return string|null The upper-case abbreviation of the flavor brand
     */
    public function getFlavorBrand(): ?string;

    /**
     * Return the percent (0-100)
     * 
     * @return float The percent of the flavor to use
     */
    public function getPercent(): float;

    /**
     * Return the base mix used for this flavoring (usually PG100)
     * 
     * @return string|null The base mix used
     */
    public function getBase(): ?string;

    /**
     * Return the ASG in g/mL for the specific flavor, or null if not available.
     * 
     * @return float|null
     */
    public function getSpecificGravity(): ?float;

}