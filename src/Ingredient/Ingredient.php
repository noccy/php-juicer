<?php

namespace NoccyLabs\Juicer\Ingredient;

class Ingredient implements IngredientInterface
{
    protected $name;

    protected $brand;

    protected $percent;

    protected $base;

    /**
     * Ingredient constructor
     * 
     * @param string The name of the ingredient
     * @param string|null The brand of the ingredient
     * @param float Percent
     * @param string The base mix (Default PG100)
     */
    public function __construct(string $name, ?string $brand=null, float $percent=0.0, string $base="PG100")
    {
        $this->name = $name;
        $this->brand = $brand;
        $this->percent = $percent;
        $this->base = $base;
    }

    /**
     * {@inheritDoc}
     */
    public function getFlavorName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritDoc}
     */
    public function getFlavorBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * {@inheritDoc}
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     * {@inheritDoc}
     */
    public function getBase(): ?string
    {
        return $this->base;
    }

    /**
     * {@inheritDoc}
     */
    public function getSpecificGravity(): ?float
    {
        return null;
    }

}