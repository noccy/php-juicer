<?php

namespace NoccyLabs\Juicer\Ingredient;

class NicotineBase
{

    const MASS_NICOTINE = 1.01;

    protected $base;

    protected $strength;

    /**
     * NicotineBase constructor
     * 
     * @param Base The base mix of the nicotine base
     * @param int The strength of the nic base (eg. 18)
     */
    public function __construct(Base $base, int $strength)
    {
        $this->base = $base;
        $this->strength = $strength;
    }

    /**
     * Get the Apparent Specific Gravity of the nicotine mixture taking
     * in account the base mix and nicotine strength.
     * 
     * @return float
     */
    public function getSpecificGravity(): float
    {

        // 18mg/ml is 1.8% nicotine
        $nicotinePercent = $this->strength / 1000;

        // Figure out how much the remaining percent is
        $baseRemain = 1.0 - $nicotinePercent;

        $base = $this->base->getComponents();
        $remainVg = $baseRemain * ($base['VG'] / 100);
        $remainPg = $baseRemain * ($base['PG'] / 100);

        $sgNic = $nicotinePercent * self::MASS_NICOTINE;
        $sgVg = $remainVg * Base::MASS_VG;
        $sgPg = $remainPg * Base::MASS_PG;

        return $sgNic + $sgVg + $sgPg;

    }

}

