<?php

namespace NoccyLabs\Juicer\Recipe\Mixer;

use NoccyLabs\Juicer\Ingredient\Base;
use NoccyLabs\Juicer\Recipe\RecipeInterface;

class MixedRecipe
{
    /** @var RecipeInterface The recipe that was mixed */
    protected $recipe;

    protected $mixedIngredients = [];

    protected $base;

    protected $volume;

    protected $nicotine;

    protected $totalFlavorMl;

    protected $totalFlavorPercent;

    protected $totalFlavorGrams;

    protected $specificGravity;

    public function __construct(RecipeInterface $recipe, array $mixedIngredients, Base $base, int $volume, int $nicotine)
    {
        $this->recipe = $recipe;
        $this->mixedIngredients = $mixedIngredients;
        $this->base = $base;
        $this->volume = $volume;
        $this->nicotine = $nicotine;

        foreach ($mixedIngredients as $mixedIngredient) {
            if (!in_array($mixedIngredient->getFlavorName(), ['PG','VG'])) {
                $this->totalFlavorMl += $mixedIngredient->getVolume();
                $this->totalFlavorGrams += $mixedIngredient->getWeight();
                $this->totalFlavorPercent += $mixedIngredient->getPercent();
            }
            $this->specificGravity += $mixedIngredient->getSpecificGravity() * ($mixedIngredient->getPercent() / 100);
        }
    }

    public function getIngredients(): array
    {
        return $this->recipe->getIngredients();
    }

    public function getBase(): Base
    {
        return $this->base;
    }

    public function getVolume(): int
    {
        return $this->volume;
    }

    public function getMeasuredIngredients(): array
    {
        return $this->mixedIngredients;
    }

    public function getTotalFlavorMl(): float
    {
        return $this->totalFlavorMl;
    }

    public function getTotalFlavorPercent(): float
    {
        return $this->totalFlavorPercent;
    }

    public function getTotalFlavorGrams(): float
    {
        return $this->totalFlavorGrams;
    }

    public function getSpecificGravity(): float
    {
        return $this->specificGravity;
    }
}