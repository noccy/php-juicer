<?php

namespace NoccyLabs\Juicer\Recipe\Mixer;

use NoccyLabs\Juicer\Ingredient\IngredientInterface;
use NoccyLabs\Juicer\Ingredient\Base;

class MeasuredIngredient implements IngredientInterface
{
    /** @var string */
    protected $name;
    /** @var string|null */
    protected $brand;
    /** @var string */
    protected $base;
    /** @var float The apparent specific gravity (ASG) of this ingredient in g/mL */
    protected $asg;
    /** @var float The percent of this ingredient in the final mix */
    protected $pecent;
    /** @var float Volume in milliliters (mL) */
    protected $volume;
    /** @var float Weight in grams (g) */
    protected $weight;

    /**
     * MeasuredIngredient constructor
     * 
     * @param IngredientInterface|string The ingredient 
     * @param float The percent of the ingredient to use
     * @param float The volume of the ingredient to mix (in mL)
     * @param string The base of the ingredient, override any set on ingredient
     */
    public function __construct($ingredient, float $percent, float $volume, string $base=null)
    {
        if ($ingredient instanceof IngredientInterface) {
            $asg = $ingredient->getSpecificGravity();
            $this->name = $ingredient->getFlavorName();
            $this->brand = $ingredient->getFlavorBrand();
            $this->base = $base??$ingredient->getBase();
        } else {
            $asg = null;
            $this->name = $ingredient;
            $this->base = $base??"PG100";
        }

        if (!$asg) {
            $base = new Base($this->base);
            $asg = $base->getSpecificGravity();
        }

        $this->asg = $asg;
        $this->percent = $percent;
        $this->volume = $volume;
        $this->weight = $volume * $asg;
    }

    /**
     * 
     * @return string
     */
    public function getFlavorName(): string
    {
        return $this->name;
    }

    /**
     * 
     * @return string|null
     */
    public function getFlavorBrand(): ?string
    {
        return $this->brand;
    }

    /**
     * 
     * @return string|null
     */
    public function getBase(): ?string
    {
        return $this->base;
    }

    /**
     * 
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     * 
     * @return float
     */
    public function getSpecificGravity(): float
    {
        return $this->asg;
    }

    /**
     * 
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * 
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }
}