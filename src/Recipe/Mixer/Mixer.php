<?php

namespace NoccyLabs\Juicer\Recipe\Mixer;

use NoccyLabs\Juicer\Ingredient\IngredientInterface;
use NoccyLabs\Juicer\Recipe\RecipeInterface;
use NoccyLabs\Juicer\Ingredient\Base;
use NoccyLabs\Juicer\Ingredient\NicotineBase;

class Mixer
{

    public function mixRecipe(RecipeInterface $recipe, int $volume, Base $base, int $nicotineStrength, ?NicotineBase $nicotineBase=null): MixedRecipe
    {
        // Array holding our final list
        $mixed = [];

        $targetVg = 0;
        $targetPg = 0;

        $components = $base->getComponents();
        
        if (array_key_exists('VG', $components) && $components['VG'] > 0) {
            $targetVg = $components['VG'];
        }

        if (array_key_exists('PG', $components) && $components['PG'] > 0) {
            $targetPg = $components['PG'];
        }

        $addedVg = 0;
        $addedPg = 0;

        foreach ($recipe->getIngredients() as $ingredient) {
            $ingredientBase = new Base($ingredient->getBase());
            $ingredientPercent = $ingredient->getPercent();
            $floatPercent = $ingredientPercent / 100;
            $addedVg += $floatPercent * $ingredientBase->getComponentPercent('VG');
            $addedPg += $floatPercent * $ingredientBase->getComponentPercent('PG');
        }

        if ($targetVg > $addedVg) {
            $remainingVg = max(0, $targetVg - $addedVg);
            $mixed[] = new MeasuredIngredient("VG", $remainingVg, $volume * ($remainingVg/100), "VG100");
        }
        if ($targetPg > $addedPg) {
            $remainingPg = max(0, $targetPg - $addedPg);
            $mixed[] = new MeasuredIngredient("PG", $remainingPg, $volume * ($remainingPg/100), "PG100");
        }

        foreach ($recipe->getIngredients() as $ingredient) {
            $ingredientBase = new Base($ingredient->getBase());
            $ingredientPercent = $ingredient->getPercent();
            $floatPercent = $ingredientPercent / 100;
            $mixed[] = new MeasuredIngredient($ingredient, $ingredientPercent, $volume * $floatPercent);
        }

        $mixedRecipe = new MixedRecipe($recipe, $mixed, $base, $volume, $nicotineStrength);
        return $mixedRecipe;
    }

}