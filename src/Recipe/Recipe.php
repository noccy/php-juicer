<?php

namespace NoccyLabs\Juicer\Recipe;

use NoccyLabs\Juicer\Ingredient\IngredientInterface;

class Recipe implements RecipeInterface
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $author;

    /** @var string[] */
    protected $tags = [];

    /** @var array */
    protected $extra = [];

    /** @var string */
    protected $description;

    /** @var IngredientInterface[] */
    protected $ingredients = [];

    public function setRecipeName(?string $name): Recipe
    {
        $this->name = $name;
        return $this;
    }

    public function setRecipeAuthor(?string $author): Recipe
    {
        $this->author = $author;
        return $this;
    }

    public function setTags(array $tags): Recipe
    {
        $this->tags = $tags;
        return $this;
    }

    public function addTag(string $tag): Recipe
    {
        $this->tags[] = $tag;
        return $this;
    }

    public function hasTag(string $tag): bool
    {
        return in_array($tag, $this->tags);
    }

    public function removeTag(string $tag): Recipe
    {
        $this->tags = array_filter($this->tags, function ($input) use ($tag) {
            return $input != $tag;
        });
        return $this;
    }

    public function setDescription(?string $description): Recipe
    {
        $this->description = $description;
        return $this;
    }

    public function addIngredient(IngredientInterface $ingredient): Recipe
    {
        $this->ingredients[] = $ingredient;
        return $this;
    }

    public function setExtra(array $extra): Recipe
    {
        $this->extra = $extra;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRecipeName(): ?string
    {
        return $this->name;
    }

    /**
     * {@inheritDoc}
     */
    public function getRecipeAuthor(): ?string
    {
        return $this->author;    
    }

    /**
     * {@inheritDoc}
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * {@inheritDoc}
     */
    public function getExtra(): array
    {
        return $this->extra;
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * {@inheritDoc}
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }

}