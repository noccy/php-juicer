<?php

namespace NoccyLabs\Juicer\Recipe\Importer;

use NoccyLabs\Juicer\Recipe\RecipeInterface;

interface ImporterInterface
{

    public function readFromFile(string $filename): RecipeInterface;

    public function import(string $data): RecipeInterface;


}
