<?php

namespace NoccyLabs\Juicer\Recipe;

use NoccyLabs\Juicer\Ingredient\IngredientInterface;

interface RecipeInterface
{

    public function getRecipeName(): ?string;

    public function getRecipeAuthor(): ?string;

    public function getTags(): array;

    public function getDescription(): ?string;

    public function getExtra(): array;

    /**
     * Get an array containing the ingredients of the recipe.
     * 
     * @return IngredientInterface[] The ingredients
     */
    public function getIngredients(): array;

}