# NoccyLabs Juicer

This is a library to help write juice-mixing stuff in PHP.

## Features

* Mix by weight or by volume.
* Calculate weights for mixing based on the VG/PG ratio or a specific values.
* Calculate weights for nicotine based on the nicotine strength and base.
* Interfaces allow for custom entity-backed implementations.


