<?php

require_once __DIR__."/../vendor/autoload.php";

use NoccyLabs\Juicer\Ingredient\Base;
use NoccyLabs\Juicer\Ingredient\NicotineBase;

printf("Apparent Specific Gravity of base mixes:\n\n");
printf("   VG   | PG   | ASG\n");
printf("  ------|------|------------------------\n");
for ($pg = 0; $pg <= 100; $pg += 10) {
    $base = new Base("PG{$pg}");
    printf("   %3d%% | %3d%% | %.4fg/mL\n", 100-$pg, $pg, $base->getSpecificGravity());
}

echo "\n\n";

$asgstrength = [ 24, 20, 18, 15, 0 ];
$asgheader = join("", array_map(function ($s) { return sprintf("%2dmg    ", $s); } , $asgstrength));

printf("Apparent Specific Gravity of nicotine bases:\n\n");
printf("   VG   | PG   | %s\n", $asgheader);
printf("  ------|------|-------------------------------------\n");
for ($pg = 0; $pg <= 100; $pg += 10) {
    printf("   %3d%% | %3d%% |", 100-$pg, $pg);
    $base = new Base("PG{$pg}");
    foreach($asgstrength as $s) {
        $nicbase = new NicotineBase($base, $s);
         printf(" %6.4f ", $nicbase->getSpecificGravity());
    }
    printf("\n");
}
