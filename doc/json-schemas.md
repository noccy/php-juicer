# Json Schemas

Juicer comes with a number of schemas to validate data.

## Definitions

**Flavoring Key**:
    A combination of the BrandKey and a unique combination of letters and
    optional version info. Ex. `FW-VBIC` for FlavorWest Vanilla Bean Ice
    Cream, or `TFA-MSHM-DX` for TFA DX Marshmallow. Max length is 32 chars.

## Conventions

* Flavoring keys always start with a vendor prefix of 2-4 characters.
* Variation of a flavor are appended, such as `DX` or `V2`
* Flavoring names should follow existing use as far as possible, while
  making it easy to associate similar flavorings from various vendors.

Exammples:

    FW-VBIC         FlavorWest          Vanilla Bean Ice Cream
    TFA-VBIC        The Flavor Artisan  Vanilla Bean Ice Cream
    CAP-VCUS        Capella             Vanilla Custard
    CAP-VCUS-V2     Capella             Vanilla Custard V2
    TFA-MSHM        The Flavor Artisan  Marshmallow
    TFA-MSHM-DX     The Flavor Artisan  Marshmallow DX
    FA-FUJA         FlavourArt          Fuji Apple

## ingredient.schema.json

Defines an ingredient.

    {
        "flavoringKey": "TFA-WCHO",
        "brandKey": "TFA",
        "brandName": "The Flavor Apprentice",
        "flavoringName": "White Chocolate",
        "ingredients": [
            "Ethyl Alcohol",
            "Vanillin",
            "Acetoin"
        ],
        "attributes": {
            "suggestedPercent": 2,
            "suggestedSteep": 7,
            "specificGravity": 1.004,
            "base": "PG100",
            "isAdditive": false
        },
        "flavorProfiles": [
            "White Chocolate"
        ],
        "warnings": [],
        "notes": []
    }

## recipe.schema.json

Defines a recipe

    {
        "url": "https://vape.noccy.com/recipe/tasty",
        "recipeName": "Tasty",
        "author": "Noccy",
        "recommendedSteep": 0,
        "ingredients": [
            {
                "flavor": "FW Vanilla Bean Ice Cream",
                "percent": 1.5
            },
            {
                "flavor": "FW Real Lemonade",
                "percent": 1
            }
        ]
    }

